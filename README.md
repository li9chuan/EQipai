# EQipai
一个用Egret引擎开发的棋牌游戏
 
**QQ群：753357671** 

主要使用技术：
- 客户端使用Egret引擎开发
- 客户端和服务器交互的协议使用websocket
- 服务器使用Lua+C++开发

服务器项目地址[传送门](https://gitee.com/li9chuan/EQipaiServer)
目前状态是开发中，感兴趣的朋友可以点个star或者fork